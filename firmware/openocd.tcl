source [find interface/stlink-v2-1.cfg]
transport select hla_swd
reset_config srst_only srst_nogate connect_assert_srst
 
source [find target/stm32f1x.cfg]

$_TARGETNAME configure -event gdb-attach {
    reset halt
}
