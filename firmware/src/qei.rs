use core::convert::Infallible;
use embedded_hal::digital::v2::InputPin;

use crate::consts::RTC_FREQ;
use crate::timer::RtcFine;

// Generic over pins
pub struct Qei<I1, I2>
where
    I1: InputPin<Error = Infallible>,
    I2: InputPin<Error = Infallible>,
{
    pin1: I1,
    pin2: I2,
    trigger: bool,
    last_coarse_time: u32,
    last_fine_time: u32,
}

// 0.1 and 0.2 second cnt of the fine time
const POINT_1_CNT: u32 = RTC_FREQ / 10;
const POINT_2_CNT: u32 = RTC_FREQ / 5;

impl<I1, I2> Qei<I1, I2>
where
    I1: InputPin<Error = Infallible>,
    I2: InputPin<Error = Infallible>,
{
    pub fn new(pin1: I1, pin2: I2, rtc: &mut RtcFine) -> Self {
        Qei {
            pin1,
            pin2,
            trigger: false,
            last_coarse_time: rtc.coarse_time(),
            last_fine_time: rtc.fine_time(),
        }
    }

    // my generic quadrature encoder function, used for multiple pins
    // this may need to end up being a macro, not sure if we can use different
    // timers with the same code :/

    //     T1   T2   T1
    //   --+    +----+
    // A   |    |    |
    //     +----+    +-
    //   +----+    +---
    // B |    |    |
    //   +    +----+
    //   TT FT FF FT
    // Trigerred on both falling and rising A, so we can look at B value to see
    // which trigger we're at and A value to see which direction
    pub fn qei(&mut self, rtc: &mut RtcFine) -> Option<u8> {
        let a_high = self.pin1.is_high().unwrap();

        // depending on whether B is high we know whether we're at T1 or T2
        let new_trigger = self.pin2.is_high().unwrap();

        // If we're at the same trigger we've just gone back so return nothing
        let fine_time = rtc.fine_time();
        let coarse_time = rtc.coarse_time();
        if new_trigger == self.trigger {
            self.last_fine_time = fine_time;
            self.last_coarse_time = coarse_time;
            return None;
        }
        // read in time to get change in time
        let time;
        let overflow;
        if coarse_time == self.last_coarse_time {
            time = self.last_fine_time - fine_time;
            overflow = false;
        } else if coarse_time == self.last_coarse_time + 1
            && fine_time < self.last_fine_time
        {
            time = fine_time - self.last_fine_time;
            overflow = false;
        } else {
            time = 0;
            overflow = true;
        }
        // update trigger and last times
        self.trigger = !self.trigger;
        self.last_fine_time = fine_time;
        self.last_coarse_time = coarse_time;

        // TODO maybe make this more granular in future
        // Sends 3 different values depending on overflow and time
        let magnitude = if overflow || time >= POINT_2_CNT {
            1
        } else if time > POINT_1_CNT && time < POINT_2_CNT {
            2
        } else {
            3
        };

        // figure out which way we've gone
        let clock_dir = new_trigger != a_high;
        let val = if clock_dir {
            60u8 + magnitude
        } else {
            60u8 - magnitude
        };

        Some(val)
    }
}
