pub const HSE_FREQ: u32 = 8_000_000;

pub const RTC_FREQ: u32 = HSE_FREQ / 256;
