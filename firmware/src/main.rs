#![no_std]
#![no_main]

use core::convert::Infallible;
use core::fmt::Debug;

use cortex_m::iprintln;
use embedded_hal::digital::v2::InputPin;
use embedded_hal::digital::v2::OutputPin;
use panic_itm as _;
use stm32f1xx_hal::usb::UsbBusType;
use stm32f1xx_hal::{
    gpio::{gpioa::*, gpiob::*, gpioc::*, Input, Output, PullUp, PushPull},
    prelude::*,
//    delay::Delay,
    stm32,
};

use usb_device::bus;
use usb_device::prelude::*;
use usbd_midi::midi_device::MidiClass;

use rtic::app;

mod consts;
mod interrupts;
mod midi;
mod qei;
mod timer;

use consts::HSE_FREQ;
use qei::Qei;
use timer::RtcFine;

use midi::*;

// Macro to check which interrupt has been set, and queue send_midi task, and turn off interrupt if
// set
macro_rules! extin_send_midi {
    ($pr: ident, $qei: ident, $num: literal, $ctx: expr, $spawn: expr, $rtc: expr) => {
        if $ctx.resources.exti.pr.read().$pr().bits() {
            let $qei = $ctx.resources.$qei;
            my_send_midi($qei, |x| $spawn.send_midi($num, x), &mut $rtc);
            $ctx.resources.exti.pr.write(|w| w.$pr().set_bit());
        }
    };
}

// macro to set up pins and return Qei struct
macro_rules! setup_qei_pins {
    ($pa: ident, $pb: ident, $porta: expr, $portb: expr, $cra: ident, $crb: ident, $rtc: expr) => {{
        let $pa = $porta.$pa.into_pull_up_input(&mut $porta.$cra);
        let $pb = $portb.$pb.into_pull_up_input(&mut $portb.$crb);
        Qei::new($pa, $pb, &mut $rtc)
    }};
}

#[app(device=crate::stm32, peripherals = true)]
const APP: () = {
    struct Resources {
        itm: stm32::ITM,
        exti: stm32::EXTI,

        on_led: PC6<Output<PushPull>>,

        // pins and bool for ext0
        rtc: RtcFine,
        qei0: Qei<PA15<Input<PullUp>>, PC10<Input<PullUp>>>,
        qei1: Qei<PA10<Input<PullUp>>, PA9<Input<PullUp>>>,
        qei2: Qei<PA8<Input<PullUp>>, PC9<Input<PullUp>>>,
        qei3: Qei<PB14<Input<PullUp>>, PB15<Input<PullUp>>>,
        qei4: Qei<PB6<Input<PullUp>>, PB7<Input<PullUp>>>,
        qei5: Qei<PC12<Input<PullUp>>, PC11<Input<PullUp>>>,
        qei6: Qei<PB11<Input<PullUp>>, PB10<Input<PullUp>>>,
        qei7: Qei<PB13<Input<PullUp>>, PB12<Input<PullUp>>>,
        qei8: Qei<PC2<Input<PullUp>>, PC3<Input<PullUp>>>,
        qei9: Qei<PB9<Input<PullUp>>, PB8<Input<PullUp>>>,
        qei10: Qei<PA5<Input<PullUp>>, PA4<Input<PullUp>>>,
        qei11: Qei<PC4<Input<PullUp>>, PC5<Input<PullUp>>>,
        qei12: Qei<PC0<Input<PullUp>>, PC1<Input<PullUp>>>,
        qei13: Qei<PA1<Input<PullUp>>, PA0<Input<PullUp>>>,
        qei14: Qei<PA3<Input<PullUp>>, PA2<Input<PullUp>>>,
        qei15: Qei<PA7<Input<PullUp>>, PA6<Input<PullUp>>>,

        // USB interfaces
        usb_dev: UsbDevice<'static, UsbBusType>,
        midi: MidiClass<'static, UsbBusType>,
    }

    #[init]
    fn init(cx: init::Context) -> init::LateResources {
        let mut cp = cx.core;
        let dp = cx.device;

        // This is needed to be able to run flashed code for some reason
        cp.DCB.enable_trace();

        // set up clocks
        let mut rcc = dp.RCC.constrain();
        let mut flash = dp.FLASH.constrain();
        let clocks = rcc
            .cfgr
            .use_hse(HSE_FREQ.hz())
            .sysclk((HSE_FREQ * 9).hz())
            .pclk1(24.mhz())
            .freeze(&mut flash.acr);

        let mut itm = cp.ITM;
        let mut exti = dp.EXTI;

        let mut porta = dp.GPIOA.split(&mut rcc.apb2);
        let mut portb = dp.GPIOB.split(&mut rcc.apb2);
        let mut portc = dp.GPIOC.split(&mut rcc.apb2);

        // disable JTAG, on by default so we have to grab the pins from there
        // Still want pb3 for itm debug
        let mut afio = dp.AFIO.constrain(&mut rcc.apb2);
        let (pa15, _pb3, _pb4) =
            afio.mapr.disable_jtag(porta.pa15, portb.pb3, portb.pb4);

        interrupts::setup(&mut exti, &mut afio);

        // set up timers for up counting
        // let tim1 = setup_timer!(&mut rcc.apb1, dp, TIM1);
        let mut pwr = dp.PWR;
        let mut backup_domain =
            rcc.bkp.constrain(dp.BKP, &mut rcc.apb1, &mut pwr);
        let mut rtc = RtcFine::new(dp.RTC, &mut backup_domain);

        // We can use all pins bar, pa11 and pa12 for usb, pa13 and pa14 for the
        // st link, and pb3 for itm out,
        let qei0 = Qei::new(
            pa15.into_pull_up_input(&mut porta.crh),
            portc.pc10.into_pull_up_input(&mut portc.crh),
            &mut rtc
        );
        let qei1 = setup_qei_pins!(pa10, pa9, porta, porta, crh, crh, rtc);
        let qei2 = setup_qei_pins!(pa8, pc9, porta, portc, crh, crh, rtc);
        let qei3 = setup_qei_pins!(pb14, pb15, portb, portb, crh, crh, rtc);
        let qei4 = setup_qei_pins!(pb6, pb7, portb, portb, crl, crl, rtc);
        let qei5 = setup_qei_pins!(pc12, pc11, portc, portc, crh, crh, rtc);
        let qei6 = setup_qei_pins!(pb11, pb10, portb, portb, crh, crh, rtc);
        let qei7 = setup_qei_pins!(pb13, pb12, portb, portb, crh, crh, rtc);
        let qei8 = setup_qei_pins!(pc2, pc3, portc, portc, crl, crl, rtc);
        let qei9 = setup_qei_pins!(pb9, pb8, portb, portb, crh, crh, rtc);
        let qei10 = setup_qei_pins!(pa5, pa4, porta, porta, crl, crl, rtc);
        let qei11 = setup_qei_pins!(pc4, pc5, portc, portc, crl, crl, rtc);
        let qei12 = setup_qei_pins!(pc0, pc1, portc, portc, crl, crl, rtc);
        let qei13 = setup_qei_pins!(pa1, pa0, porta, porta, crl, crl, rtc);
        let qei14 = setup_qei_pins!(pa3, pa2, porta, porta, crl, crl, rtc);
        let qei15 = setup_qei_pins!(pa7, pa6, porta, porta, crl, crl, rtc);

        // Only safe to do during initisation
        let (midi, usb_dev) = unsafe {
            usb_midi_init(
                porta.pa11,
                porta.pa12,
                dp.USB,
                &mut porta.crh,
                &clocks,
            )
        };
        let mut usb_pull_up = portc.pc7.into_push_pull_output(&mut portc.crl);
        usb_pull_up.set_high().unwrap();

        // set up and turn on LED to know that it's started up
        let mut on_led = portc.pc6.into_push_pull_output(&mut portc.crl);
        on_led.set_high().unwrap();

        // itm print to check our baud rates are correct
        iprintln!(&mut itm.stim[0], "Set up chip");

        init::LateResources {
            itm,
            exti,
            on_led,
            // clock
            rtc,
            // Quadrature pin structures
            qei0,
            qei1,
            qei2,
            qei3,
            qei4,
            qei5,
            qei6,
            qei7,
            qei8,
            qei9,
            qei10,
            qei11,
            qei12,
            qei13,
            qei14,
            qei15,
            // USB
            usb_dev,
            midi,
        }
    }

    #[task(binds=EXTI0, spawn = [send_midi], resources=[exti, qei12, rtc])]
    fn exti0(ctx: exti0::Context) {
        let mut rtc = ctx.resources.rtc;
        let spawn = ctx.spawn;
        let qei12 = ctx.resources.qei12;
        my_send_midi(qei12, |x| spawn.send_midi(12, x), &mut rtc);
        ctx.resources.exti.pr.write(|w| w.pr0().set_bit());
    }

    #[task(binds=EXTI1, spawn = [send_midi], resources=[exti, qei13, rtc])]
    fn exti1(ctx: exti1::Context) {
        let mut rtc = ctx.resources.rtc;
        let spawn = ctx.spawn;
        let qei13 = ctx.resources.qei13;
        my_send_midi(qei13, |x| spawn.send_midi(13, x), &mut rtc);
        ctx.resources.exti.pr.write(|w| w.pr1().set_bit());
    }

    #[task(binds=EXTI2, spawn = [send_midi], resources=[exti, qei8, rtc])]
    fn exti2(ctx: exti2::Context) {
        let spawn = ctx.spawn;
        let mut rtc = ctx.resources.rtc;
        let qei8 = ctx.resources.qei8;
        my_send_midi(qei8, |x| spawn.send_midi(8, x), &mut rtc);
        ctx.resources.exti.pr.write(|w| w.pr2().set_bit());
    }

    #[task(binds=EXTI3, spawn = [send_midi], resources=[exti, rtc, qei14])]
    fn exti3(ctx: exti3::Context) {
        let spawn = ctx.spawn;
        let mut rtc = ctx.resources.rtc;
        let qei14 = ctx.resources.qei14;
        my_send_midi(qei14, |x| spawn.send_midi(14, x), &mut rtc);
        ctx.resources.exti.pr.write(|w| w.pr3().set_bit());
    }

    #[task(binds=EXTI4, spawn = [send_midi], resources=[exti, qei11, rtc])]
    fn exti4(ctx: exti4::Context) {
        let spawn = ctx.spawn;
        let mut rtc = ctx.resources.rtc;
        let qei11 = ctx.resources.qei11;
        my_send_midi(qei11, |x| spawn.send_midi(11, x), &mut rtc);
        ctx.resources.exti.pr.write(|w| w.pr4().set_bit());
    }

    #[task(binds=EXTI9_5, spawn = [send_midi],
           resources=[exti, rtc, qei10, qei4, qei15, qei2, qei9])]
    fn exti9_5(ctx: exti9_5::Context) {
        let spawn = ctx.spawn;
        let mut rtc = ctx.resources.rtc;
        extin_send_midi!(pr5, qei10, 10, ctx, spawn, rtc);
        extin_send_midi!(pr6, qei4, 4, ctx, spawn, rtc);
        extin_send_midi!(pr7, qei15, 15, ctx, spawn, rtc);
        extin_send_midi!(pr8, qei2, 2, ctx, spawn, rtc);
        extin_send_midi!(pr9, qei9, 9, ctx, spawn, rtc);
    }

    #[task(binds=EXTI15_10, spawn = [send_midi],
           resources=[exti, rtc, qei1, qei6, qei5, qei7, qei3, qei0])]
    fn exti15_10(ctx: exti15_10::Context) {
        let spawn = ctx.spawn;
        let mut rtc = ctx.resources.rtc;
        extin_send_midi!(pr10, qei1, 1, ctx, spawn, rtc);
        extin_send_midi!(pr11, qei6, 6, ctx, spawn, rtc);
        extin_send_midi!(pr12, qei5, 5, ctx, spawn, rtc);
        extin_send_midi!(pr13, qei7, 7, ctx, spawn, rtc);
        extin_send_midi!(pr14, qei3, 3, ctx, spawn, rtc);
        extin_send_midi!(pr15, qei0, 0, ctx, spawn, rtc);
    }

    // Also taken from rust midi stomp
    /// Sends a midi message over the usb bus
    /// Note: this runs at a lower priority than the usb bus
    /// and will eat messages if the bus is not configured yet
    #[task(priority=2, resources=[usb_dev, midi, itm])]
    fn send_midi(ctx: send_midi::Context, chan: u8, val: u8) {
        let mut midi = ctx.resources.midi;
        let mut usb_dev = ctx.resources.usb_dev;
        let itm = ctx.resources.itm;

        let packet = create_cc_msg(chan, val);

        // Lock this so USB interrupts don't take over
        // Ideally we may be able to better determine this, so that
        // it doesn't need to be locked
        let mut res = Err(UsbError::InvalidState);
        usb_dev.lock(|usb_dev| {
            if usb_dev.state() == UsbDeviceState::Configured {
                midi.lock(|midi| {
                    res = midi.send_message(packet);
                })
            }
        });
        match res {
            Ok(_) => iprintln!(&mut itm.stim[0], "Sent {} {}", chan, val),
            Err(e) => {
                iprintln!(&mut itm.stim[0], "failed to send midi: {:?}", e)
            }
        }
    }

    // Tasks for usb stuff, without these the device does not gete recognized
    #[task(binds = USB_HP_CAN_TX, resources = [usb_dev, midi], priority=3)]
    fn usb_tx(mut cx: usb_tx::Context) {
        usb_poll(&mut cx.resources.usb_dev, &mut cx.resources.midi);
    }

    #[task(binds = USB_LP_CAN_RX0, resources = [usb_dev, midi], priority=3)]
    fn usb_rx0(mut cx: usb_rx0::Context) {
        usb_poll(&mut cx.resources.usb_dev, &mut cx.resources.midi);
    }

    // Required for software tasks
    extern "C" {
        // Uses the DMA1_CHANNELX interrupts for software
        // task scheduling.
        fn DMA1_CHANNEL1();
    }
};

fn usb_poll<B: bus::UsbBus>(
    usb_dev: &mut UsbDevice<'static, B>,
    midi: &mut MidiClass<'static, B>,
) {
    if !usb_dev.poll(&mut [midi]) {
        return;
    }
}

fn my_send_midi<I1, I2, F, E>(qei: &mut Qei<I1, I2>, f: F, rtc: &mut RtcFine)
where
    I1: InputPin<Error = Infallible>,
    I2: InputPin<Error = Infallible>,
    F: FnOnce(u8) -> Result<(), E>,
    E: Debug,
{
    let val = qei.qei(rtc);
    match val {
        Some(val) => match f(val as u8) {
            Ok(_) => (),
            Err(err) => panic!("Failed to spawn midi task: {:?}", err),
        },
        None => (),
    }
}
