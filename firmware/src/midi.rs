use cortex_m::asm::delay;

use embedded_hal::digital::v2::OutputPin;
use stm32f1xx_hal::gpio::{gpioa::*, *};
use stm32f1xx_hal::pac::USB;
use stm32f1xx_hal::rcc::Clocks;
use stm32f1xx_hal::usb::{Peripheral, UsbBus, UsbBusType};
use usb_device::{
    bus,
    device::{UsbDevice, UsbDeviceBuilder, UsbVidPid},
};
use usbd_midi::{
    data::{
        byte::from_traits::FromOverFlow,
        byte::u7::*,
        midi::{channel::Channel, message::{Message as MidiMessage, control_function::ControlFunction}},
        usb::constants::USB_CLASS_NONE,
        usb_midi::{
            cable_number::CableNumber,
            usb_midi_event_packet::UsbMidiEventPacket,
        },
    },
    midi_device::MidiClass,
};

// initialisation of pins for usb midi
pub unsafe fn usb_midi_init(
    usb_dm: PA11<Input<Floating>>,
    usb_dp: PA12<Input<Floating>>,
    usb_pac: USB,
    crh: &mut CRH,
    clocks: &Clocks,
) -> (
    MidiClass<'static, UsbBusType>,
    UsbDevice<'static, UsbBusType>,
) {
    // static USB_BUS to ensure it lasts for ever
    static mut USB_BUS: Option<bus::UsbBusAllocator<UsbBusType>> = None;

    // this sets the usb_dp to low to reset the device before changing it back
    // to input, useful during for development
    let mut usb_dp = usb_dp.into_push_pull_output(crh);
    usb_dp.set_low().unwrap();
    delay(clocks.sysclk().0 / 100);
    let usb_dp = usb_dp.into_floating_input(crh);

    let usb = Peripheral {
        usb: usb_pac,
        pin_dm: usb_dm,
        pin_dp: usb_dp,
    };

    USB_BUS = Some(UsbBus::new(usb));

    let midi = MidiClass::new(USB_BUS.as_ref().unwrap());
    let usb_dev = UsbDeviceBuilder::new(
        USB_BUS.as_ref().unwrap(),
        UsbVidPid(0x0000, 0x0000),
    )
    .manufacturer("My Fake Company")
    .product("r16-midi")
    .serial_number("0")
    .device_class(USB_CLASS_NONE)
    .build();

    (midi, usb_dev)
}

pub fn create_cc_msg(chan: u8, val: u8) -> UsbMidiEventPacket {
    let midi_msg = MidiMessage::ControlChange(
        Channel::Channel1,
        ControlFunction(U7::from_overflow(chan)),
        U7::from_overflow(val),
    );
    UsbMidiEventPacket::from_midi(CableNumber::Cable1, midi_msg)
}
