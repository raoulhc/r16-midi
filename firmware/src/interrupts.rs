use stm32f1xx_hal::{stm32, afio};

macro_rules! set_bits {
    ($reg: expr, $($id: ident),+) => {
        $reg.write(|w| w.$($id().set_bit()).+);
        }
}

/// Function to set up interrupts
pub fn setup(exti: &mut stm32::EXTI, afio: &mut afio::Parts) {
    // Set all 16 triggers on, and trigger on rising and falling
    set_bits!(
        exti.imr, mr0, mr1, mr2, mr3, mr4, mr5, mr6, mr7, mr8, mr9, mr10, mr11,
        mr12, mr13, mr14, mr15
    );
    set_bits!(
        exti.rtsr, tr0, tr1, tr2, tr3, tr4, tr5, tr6, tr7, tr8, tr9, tr10,
        tr11, tr12, tr13, tr14, tr15
    );
    set_bits!(
        exti.ftsr, tr0, tr1, tr2, tr3, tr4, tr5, tr6, tr7, tr8, tr9, tr10,
        tr11, tr12, tr13, tr14, tr15
    );

    // set the correct lines
    unsafe {
        afio.exticr1.exticr1().write(
            |w| w
                .exti0().bits(2)
                .exti1().bits(0)
                .exti2().bits(2)
                .exti3().bits(0)
        );
        afio.exticr2.exticr2().write(
            |w| w
                .exti4().bits(2)
                .exti5().bits(0)
                .exti6().bits(1)
                .exti7().bits(0)
        );
        afio.exticr3.exticr3().write(
            |w| w
                .exti8().bits(0)
                .exti9().bits(1)
                .exti10().bits(0)
                .exti11().bits(1)
        );
        afio.exticr4.exticr4().write(
            |w| w
                .exti12().bits(2)
                .exti13().bits(1)
                .exti14().bits(1)
                .exti15().bits(0)
        );
    }
}
