use stm32f1xx_hal::backup_domain::BackupDomain;
use stm32f1xx_hal::pac::{RCC, RTC};

use crate::consts::RTC_FREQ;

// This is very similar to the implementation of Rtc in the stm32f1xx_hal
// package however it uses the hse rather than lse as I may not have the latter
// on the final board, and gives access to the divider registers which allow us
// to read a much more finegrained time. This functionality should probably be
// added to the upstream Rtc eventually.
pub struct RtcFine {
    regs: RTC,
}

impl RtcFine {
    pub fn new(regs: RTC, bkp: &mut BackupDomain) -> Self {
        let mut result = RtcFine { regs };
        // Set hse clock as input source, has to be done first
        RtcFine::enable_hse_rtc(bkp);
        // Set counter to take a second
        result.perform_write(|s| {
            s.regs.prlh.write(|w| unsafe { w.bits(0b0000) });
            s.regs.prll.write(|w| unsafe { w.bits(RTC_FREQ) });
        });
        result
    }

    fn enable_hse_rtc(_: &mut BackupDomain) {
        // NOTE: Safe RCC access because we are only accessing bdcr
        // and we have a &mut on BackupDomain
        let rcc = unsafe { &*RCC::ptr() };
        rcc.bdcr.modify(|_, w| {
            w
                // Set the source of the RTC to HSE
                .rtcsel()
                .hse()
                // Enable the RTC
                .rtcen()
                .set_bit()
        })
    }

    pub fn coarse_time(&self) -> u32 {
        while !self.regs.crl.read().rsf().bit() {}
        let cnth = self.regs.cnth.read().bits();
        let cntl = self.regs.cntl.read().bits();
        cnth << 16 | cntl
    }

    pub fn fine_time(&self) -> u32 {
        while !self.regs.crl.read().rsf().bit() {}
        let divh: u32 = self.regs.divh.read().bits();
        let divl: u32 = self.regs.divl.read().bits();
        divh << 16 | divl
    }

    /**
    The RTC registers can not be written to at any time as documented on page
    485 of the manual. Performing writes using this function ensures that
    the writes are done correctly.
     */
    fn perform_write(&mut self, func: impl Fn(&mut Self)) {
        // Wait for the last write operation to be done
        while !self.regs.crl.read().rtoff().bit() {}
        // Put the clock into config mode
        self.regs.crl.modify(|_, w| w.cnf().set_bit());

        // Perform the write operation
        func(self);

        // Take the device out of config mode
        self.regs.crl.modify(|_, w| w.cnf().clear_bit());
        // Wait for the write to be done
        while !self.regs.crl.read().rtoff().bit() {}
    }
}
