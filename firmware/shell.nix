let
  moz_overlay = import (builtins.fetchTarball
    https://github.com/mozilla/nixpkgs-mozilla/archive/master.tar.gz);
  nixpkgs = import <nixpkgs> { overlays = [ moz_overlay ]; };
  ruststable = ((nixpkgs.rustChannelOf {
    channel = "1.48.0";
  }).rust.override {
    extensions = [ "rust-src" "rust-analysis" "rustfmt-preview" "clippy-preview"];
    targets = ["thumbv7m-none-eabi"];
  });
in
  with nixpkgs;
  stdenv.mkDerivation {
    name = "rust";
    buildInputs = [
      pkgconfig
      ruststable
      latest.rustChannels.stable.cargo
      glib
      gdb
      emacs
      stlink
      gcc-arm-embedded
      openocd
    ];
  }

