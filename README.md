# r16-midi

This source for is for a simple midi controller that has 16 rotary encoders,
with values sent based on the direction and speed it was turned.

This is achieved with an stm32f103c8t6, setting up 16 rising and falling
triggered interrupts on one of the pins of each encoder, and checking both pin
values to get the direction, and then getting the RTC value to see how much time
has passed since the last interrupt.

This using the rtic framework allowing simple set up of interrupts for encoders
and tasks to send midi messages.

WIP! Channels, and values sent subject to change.

## Hardware

The circuitry has been designed in the KiCad version 5.1.6, using digikeys
kicad footprint library for standard symbols and footprints (which can be found
[here](https://github.com/Digi-Key/digikey-kicad-library)).

## Building and running

If you have nix you can just run `nix-shell` in order to use mozillas rust
overlay get dependencies, else you'll need to ensure you have: cargo, with
target thumbv7m-none-eabi; openocd for debugging; and stlink and gnu arm
binutils in order to flash the device.

### Debugging

To debug you need to run the following openocd command in a terminal.
```
openocd -f interface/stlink-v2.cfg -f target/stm32f1x.cfg
```
updating the stlink version depending on which device you're using.

You may need to add some udev rules or run as sudo, as well as holding the reset
pin in order to ensure that openocd connects.

To do printf style debugging you can connect pb3 to tx of a UART adapter with a
common ground, to get ITM messages at a baud rate of 3000000. Using picocom you
can do something along the following lines:
```
picocom -b 3000000 /dev/ttyUSB0 --imap lfcrlf
```

### Flashing

In order to flash the device you can run the following commands.
```
cargo build --release
arm-none-eabi-objcopy -O binary target/thumbv7m-none-eabi/release/r16-midi r16-midi.bin
st-flash write r16-midi.bin 0x800000
```
This should then work without the stlink plugged in, though there will be no
itm messages outputted.
