screw_r = 75;
hole_radius = 2.5;
inner_radius = 72.4;
inner_fillet = 3.4;
outer_radius = 80;
outer_fillet = 5;

// USB cut out bit
usb_width = 9.45;
usb_height = 1.85;

// screw holes is the same on every layer
module screw_holes() {
	translate([screw_r, screw_r]) circle(hole_radius, $fn=100);
	translate([-screw_r, screw_r]) circle(hole_radius, $fn=100);
	translate([screw_r, -screw_r]) circle(hole_radius, $fn=100);
	translate([-screw_r, -screw_r]) circle(hole_radius, $fn=100);
}

module plate() {
	difference() {
		// outer limit
		minkowski() {
			square(2 * (outer_radius - outer_fillet), center=true);
			circle(r=outer_fillet, $fn=100);
		}

		// Screws holes
		screw_holes();
	}
}

module inner() {
	// inner limit
	minkowski() {
		square(2 * (inner_radius - inner_fillet), center=true);
		circle(r=inner_fillet, $fn=100);
	}
}

module usb_cutout() {

	// cutout to outer height
	polygon(points=[
		[-usb_width/2, inner_radius+usb_height],
		[-(usb_width/2 + 5), outer_radius],
		[+(usb_width/2 + 5), outer_radius],
		[+usb_width/2, inner_radius+usb_height]
	]);
}

// base
translate([outer_radius,outer_radius]) difference() {
	minkowski() {
		square(2*(outer_radius - outer_fillet), center=true);
		circle(r=outer_fillet, $fn=100);
	}
	screw_holes();
}

// 1st inner layer
translate([3*outer_radius + 2, outer_radius]) difference() {
	plate();
	inner();
	usb_cutout();
}

// 2nd inner layer, this has space for the usb 
translate([3*outer_radius+2, 3*outer_radius+2]) difference() {

	plate();
	inner();

	translate([-usb_width/2, inner_radius]) square([usb_width, usb_height]);

	// cutout to outer height
	usb_cutout();
}

// top layer
translate([outer_radius, 3*outer_radius + 2]) difference() {
	plate();

	start = -62.4;
	for (x=[0,1,2,3]) {
		for (y=[0,1,2,3]) {
			translate([start+40*x, start+40*y]) circle(r=3.6);
		}
	}

	usb_cutout();
}
