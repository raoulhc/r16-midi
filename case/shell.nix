let
  nixpkgs = import <nixpkgs> {};
in
  with nixpkgs;
  stdenv.mkDerivation {
    name = "openscad";
    buildInputs = [
      openscad
    ];
  }
